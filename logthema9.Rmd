---
title: "Log thema 9"
author: "Quint Sangers"
date: "9/10/2019"
output:
  pdf_document: default
  html_document: default
---

Downloaded data from this source:

https://archive.ics.uci.edu/ml/datasets/wine+quality

```{r}
library(ggplot2)
redwine = read.csv(file='winequality-red.csv', header=TRUE, sep=';')
whitewine = read.csv(file='winequality-white.csv', header=TRUE, sep=';')
```

#Data Analysis

class attribute: quality (integer between 0 and 10)
The quality is determined by three sensory assessors, who graded the wine on a scale from 0 to 10. The median of these grades is used as the quality score for this dataset.

```{r}
qrange <- c(1:10) #range of possible quality values

#plotting the redwine bar chart
ggplot(redwine)+
    geom_bar(aes(x=quality), fill='darkred')+
    labs(x='Quality score', y='Number of ocurrences', title='Red wine')+
    scale_x_continuous(labels=qrange, breaks=qrange)

#plotting the whitewine bar chart
ggplot(whitewine)+
    geom_bar(aes(x=quality), fill='lightyellow')+
    labs(x='Quality score', y='Number of ocurrences', title='White wine')+
    scale_x_continuous(labels=qrange, breaks=qrange)
```

amount of attributes: 11

All the attributes are obtained by measuring the physicochemical properties of the wine

```{r}


```


